from PyQt5 import QtWidgets

class Ventana(QtWidgets.QMainWindow):

    def __init__(self):
        super().__init__()
        self.setFixedSize(300, 200)                     #Seteamos el tamaño de la ventana
        self.setWindowTitle("Suma numeros")             #Le ponemos un titulo a la ventana

        self.centralWidget = QtWidgets.QWidget(self)    #Widget central de la ventana
        self.setCentralWidget(self.centralWidget)       #Seteamos el widget central

        self.layout = QtWidgets.QVBoxLayout()           #Creamos un layout para posicionar los elementos
        self.centralWidget.setLayout(self.layout)       #Seteamos el layout

        self.texto1 = QtWidgets.QLabel()                 
        self.texto1.setText("Escriba el primer numero:")         
        self.num1 = QtWidgets.QLineEdit()
        self.texto2 = QtWidgets.QLabel()                 
        self.texto2.setText("Escriba el segundo numero:")       
        self.num2 = QtWidgets.QLineEdit()
        
        self.boton = QtWidgets.QPushButton()
        self.boton.setText("Sumar")
        self.boton.clicked.connect(self.sumar)
        self.textoMuestra = QtWidgets.QLabel()
        #self.textoMuestra.setText("")

        self.layout.addWidget(self.texto1)
        self.layout.addWidget(self.num1)
        self.layout.addWidget(self.texto2)
        self.layout.addWidget(self.num2)
        self.layout.addWidget(self.boton)
        self.layout.addWidget(self.textoMuestra)

    def sumar(self):
        suma = int(self.num1.text()) + int(self.num2.text())
        self.textoMuestra.setText(str(suma))

app = QtWidgets.QApplication([])
ventana = Ventana()
ventana.show()
app.exec_()