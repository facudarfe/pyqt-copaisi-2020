from PyQt5 import QtWidgets, uic

class Formulario(QtWidgets.QMainWindow):

    def __init__(self):
        super().__init__()
        uic.loadUi('formulario.ui', self)

        self.countryCombo.addItems(sorted(['Argentina', 'Peru', 'Brasil', 'Colombia', 'Uruguay', 'Chile', 'Venezuela', 'Ecuador']))
        self.errorLabel.hide() #Escondemos el label del error
        
        #Metodos para ajustar el encabeza de la tabla. Stretch: se ajusta al tamaño disponible. ResizeToContents: Se ajusta al contenido
        header = self.tableWidget.horizontalHeader()     
        header.setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)
        header.setSectionResizeMode(1, QtWidgets.QHeaderView.Stretch)
        header.setSectionResizeMode(2, QtWidgets.QHeaderView.Stretch)
        header.setSectionResizeMode(3, QtWidgets.QHeaderView.ResizeToContents)
        header.setSectionResizeMode(4, QtWidgets.QHeaderView.ResizeToContents)

        self.enrollButton.clicked.connect(self.cargarFormulario) #Conectamos el Slot al Signal clicked

    def cargarFormulario(self):
        nombre = self.nameEdit.text()
        email = self.mailEdit.text()
        
        #Chequeamos que se hayan insertado datos
        if nombre != '' and email != '':
            pais = self.countryCombo.currentText()
            fecha = self.birthDate.date().toString()
            
            if self.studentCheckbox.isChecked():
                estudiante = 'Si'
            else:
                estudiante = 'No'
            
            self.cargarTabla(nombre, email, pais, fecha, estudiante) #Cargamos la tabla con los datos

            #Limpiamos los campos de texto y ocultamos el mensaje de error si lo hubiera
            self.nameEdit.clear()
            self.mailEdit.clear()
            self.errorLabel.hide()
            self.studentCheckbox.setChecked(False)
        else:
            self.errorLabel.show() #Si no se insertaron algunos datos se muestra mensaje de error

    def cargarTabla(self, nombre, email, pais, fecha, estudiante):
        self.tableWidget.insertRow(0) #Insertaremos los datos en la fila 0 de la tabla

        self.tableWidget.setItem(0, 0, QtWidgets.QTableWidgetItem(nombre))
        self.tableWidget.setItem(0, 1, QtWidgets.QTableWidgetItem(email))
        self.tableWidget.setItem(0, 2, QtWidgets.QTableWidgetItem(pais))
        self.tableWidget.setItem(0, 3, QtWidgets.QTableWidgetItem(fecha))
        self.tableWidget.setItem(0, 4, QtWidgets.QTableWidgetItem(estudiante))

if __name__ == '__main__':
    app = QtWidgets.QApplication([])
    ventana = Formulario()
    ventana.show()
    app.exec_()
