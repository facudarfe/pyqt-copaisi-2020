from PyQt5 import QtWidgets, uic

class VentanaSlider(QtWidgets.QMainWindow):
    
    def __init__(self):
        super().__init__()
        uic.loadUi('slider.ui', self)

        self.horizontalSlider.valueChanged.connect(self.actualizaEdit)

    def actualizaEdit(self):
        if self.checkBox.isChecked():
            self.lineEdit.setText(str(self.horizontalSlider.value()))

app = QtWidgets.QApplication([])
miVentana = VentanaSlider()
miVentana.show()
app.exec_()