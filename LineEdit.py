from PyQt5 import QtWidgets

class Ventana(QtWidgets.QMainWindow):

    def __init__(self):
        super().__init__()
        self.setFixedSize(300, 200)                     #Seteamos el tamaño de la ventana
        self.setWindowTitle("Mi primera interfaz")      #Le ponemos un titulo a la ventana

        self.centralWidget = QtWidgets.QWidget(self)    #Widget central de la ventana
        self.setCentralWidget(self.centralWidget)       #Seteamos el widget central

        self.layout = QtWidgets.QVBoxLayout()           #Creamos un layout para posicionar los elementos
        self.centralWidget.setLayout(self.layout)       #Seteamos el layout

        self.texto = QtWidgets.QLabel()                 #Creamos un label
        self.texto.setText("Esriba un texto:")          #Le ponemos un texto a ese label
        self.campo = QtWidgets.QLineEdit()
        self.boton = QtWidgets.QPushButton()
        self.boton.setText("Mostrar")
        self.boton.clicked.connect(self.mostrar)
        self.textoMuestra = QtWidgets.QLabel()
        #self.textoMuestra.setText("")

        self.layout.addWidget(self.texto)               #Añadimos el label al layout antes creado
        self.layout.addWidget(self.campo)
        self.layout.addWidget(self.boton)
        self.layout.addWidget(self.textoMuestra)

    def mostrar(self):
        self.textoMuestra.setText(self.campo.text())

app = QtWidgets.QApplication([])
ventana = Ventana()
ventana.show()
app.exec_()