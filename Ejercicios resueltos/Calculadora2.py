from PyQt5 import QtWidgets, uic

class VentanaCalculadora(QtWidgets.QMainWindow):

    def __init__(self):
        super().__init__()
        uic.loadUi('calculadora.ui', self)

        self.addButton.clicked.connect(self.resolver)
        self.subButton.clicked.connect(self.resolver)
        self.prodButton.clicked.connect(self.resolver)
        self.divButton.clicked.connect(self.resolver)

    def resolver(self):
        boton = self.sender()   #Con esta instruccion obtengo que boton fue el que se presiono

        if self.num1Edit.text() != '' and self.num2Edit.text() != '':
            if boton.text() == '+':
                self.resEdit.setText(str(int(self.num1Edit.text()) + int(self.num2Edit.text())))
            elif boton.text() == '-':
                self.resEdit.setText(str(int(self.num1Edit.text()) - int(self.num2Edit.text())))
            elif boton.text() == '*':
                self.resEdit.setText(str(int(self.num1Edit.text()) * int(self.num2Edit.text())))
            elif boton.text() == '/':
                if int(self.num2Edit.text()) != 0:
                    self.resEdit.setText(str(int(self.num1Edit.text()) / int(self.num2Edit.text())))
                else:
                    self.resEdit.setText('Error. Division por 0')

app = QtWidgets.QApplication([])
ventana = VentanaCalculadora()
ventana.show()
app.exec_()