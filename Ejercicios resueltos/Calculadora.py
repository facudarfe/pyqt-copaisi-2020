from PyQt5 import QtWidgets, uic

class VentanaCalculadora(QtWidgets.QMainWindow):

    def __init__(self):
        super().__init__()
        uic.loadUi('calculadora.ui', self)

        self.addButton.clicked.connect(self.sumar)
        self.subButton.clicked.connect(self.restar)
        self.prodButton.clicked.connect(self.multiplicar)
        self.divButton.clicked.connect(self.dividir)

    def sumar(self):
        if self.num1Edit.text() != '' and self.num2Edit.text() != '':
            self.resEdit.setText(str(int(self.num1Edit.text()) + int(self.num2Edit.text())))

    def restar(self):
        if self.num1Edit.text() != '' and self.num2Edit.text() != '':
            self.resEdit.setText(str(int(self.num1Edit.text()) - int(self.num2Edit.text())))

    def multiplicar(self):
        if self.num1Edit.text() != '' and self.num2Edit.text() != '':
            self.resEdit.setText(str(int(self.num1Edit.text()) * int(self.num2Edit.text())))

    def dividir(self):
        if self.num1Edit.text() != '' and self.num2Edit.text() != '':
            if int(self.num2Edit.text()) != 0:
                self.resEdit.setText(str(int(self.num1Edit.text()) / int(self.num2Edit.text())))
            else:
                self.resEdit.setText('Error. Division por 0')

app = QtWidgets.QApplication([])
ventana = VentanaCalculadora()
ventana.show()
app.exec_()