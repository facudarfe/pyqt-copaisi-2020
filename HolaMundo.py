from PyQt5 import QtWidgets

class Ventana(QtWidgets.QMainWindow):

    def __init__(self):
        super().__init__()
        self.setFixedSize(300, 100)                     #Seteamos el tamaño de la ventana
        self.setWindowTitle("Mi primera interfaz")      #Le ponemos un titulo a la ventana

        self.centralWidget = QtWidgets.QWidget(self)    #Widget central de la ventana
        self.setCentralWidget(self.centralWidget)       #Seteamos el widget central

        self.layout = QtWidgets.QVBoxLayout()           #Creamos un layout para posicionar los elementos
        self.centralWidget.setLayout(self.layout)       #Seteamos el layout

        self.texto = QtWidgets.QLabel()                 #Creamos un label
        self.texto.setText("Hola mundo")                #Le ponemos un texto a ese label

        self.layout.addWidget(self.texto)               #Añadimos el label al layout antes creado

app = QtWidgets.QApplication([])
ventana = Ventana()
ventana.show()
app.exec_()